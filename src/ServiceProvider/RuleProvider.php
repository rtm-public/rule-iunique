<?php

namespace RenatoMelo\Rule\ServiceProvider;

use Illuminate\Support\ServiceProvider;

class RuleProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot():void
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
