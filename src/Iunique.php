<?php

namespace RenatoMelo\Rule;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class Iunique implements ValidationRule
{

    private $model, $colunaNome, $ignore, $regexPatternDehydrate, $outrasColunas;

    public function __construct($model, $ignore, $colunaNome = null, $regexPatternDehydrate = null, $outrasColunas = [])
    {
        $this->model = $model;
        $this->colunaNome = $colunaNome;
        $this->ignore = $ignore;
        $this->regexPatternDehydrate = $regexPatternDehydrate;
        $this->outrasColunas = $outrasColunas;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $model = new $this->model;

        $coluna = $this->colunaNome ?? $attribute;

        if ($this->regexPatternDehydrate) {
            $value = preg_replace($this->regexPatternDehydrate, '', $value);
        }

        $existe = $model
            ->whereRaw("unaccent($coluna) ilike unaccent(?)", [$value])
            ->when($this->ignore, function ($query) use ($model) {
                $query->where($model->getKeyName(), '!=', $this->ignore);
            })
            ->when($this->outrasColunas, function ($query) {
                foreach (($this->outrasColunas ?? []) as $key => $valor) {
                    if (is_numeric($valor)) {
                        $query->where($key, $valor);
                    } else if ($valor === null) {
                        $query->whereNull($key);
                    } else {
                        $query->whereRaw("unaccent($key) ilike unaccent(?)", [$valor]);
                    }
                }
            })
            ->first();

        if ($existe) {
            $fail('Valor já existe!');
        }
    }

}
