# Rule Iunuque

## Para instalar
```
"repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/rtm-public/rule-iunique.git"
        }
    ]
```


` $_> composer require renatomelo/rule-iunique `

### Erro na instalação

execute antes esse comando

` $_> composer config -g repo.packagist composer https://packagist.org `


## Como usar
**` use RenatoMelo\Rule\Iunique; `**
<br> .
<br> .
<br> .

`... 'nome' => ['required', new Iunique('Modules\V1\Entities\Sistema', $this->sistema)] ...`
<br> .
<br> .
<br> .
